package com.example.file.model;

import com.example.common.topics.File;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Document
public class FileModel {

    @Id
    private String id;

    private String description;

    private String content;

    public static FileModel from(File file) {
        return FileModel.builder()
                .id(file.getId())
                .description(file.getDescription())
                .content(file.getContent())
                .build();
    }
}
