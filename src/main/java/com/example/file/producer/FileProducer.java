package com.example.file.producer;

import com.example.common.domain.Record;
import com.example.common.domain.Record.CrudType;
import com.example.common.topics.File;
import com.example.file.dao.FileDao;
import com.example.file.model.FileModel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.NotImplementedException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;

@Service
@RequiredArgsConstructor
public class FileProducer {

    private final KafkaSender<String, Record<File>> sender;

    private final FileDao fileDao;

    public Mono<Void> send(CrudType type, Mono<File> file) {
        return file
                .flatMap(f -> {
                    switch (type) {
                        case CREATE:
                            return fileDao
                                    .save(FileModel.from(f))
                                    .thenReturn(f);
                        case UPDATE:
                            return fileDao
                                    .update(f.getId(), FileModel.from(f))
                                    .thenReturn(f);
                        case DELETE:
                            return fileDao
                                    .deleteById(f.getId())
                                    .thenReturn(File.builder()
                                            .id(f.getId())
                                            .build());
                        default:
                            return Mono.error(new NotImplementedException("No Read yet :("));
                    }
                })
                .flatMap(f ->
                        sender.createOutbound()
                                .send(Flux.just(
                                        new ProducerRecord<>("file",
                                                Record.<File>builder()
                                                        .topic("file")
                                                        .type(type)
                                                        .data(f)
                                                        .build())))
                                .then());
    }
}
