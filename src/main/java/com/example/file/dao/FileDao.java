package com.example.file.dao;

import com.example.file.model.FileModel;
import com.example.file.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class FileDao {

    private final FileRepository fileRepository;

    public Mono<FileModel> save(FileModel fileModel) {
        return fileRepository.save(fileModel);
    }

    public Flux<FileModel> findAll() {
        return fileRepository.findAll();
    }

    public Mono<Void> deleteById(String id) {
        return fileRepository.deleteById(id);
    }

    public Mono<FileModel> update(String id, FileModel fileModel) {
        return fileRepository
                .findById(id)
                .flatMap(value ->
                        Mono.just(value
                                .toBuilder()
                                .description(fileModel.getDescription())
                                .content(fileModel.getContent())
                                .build()))
                .flatMap(fileRepository::save);
    }

}
