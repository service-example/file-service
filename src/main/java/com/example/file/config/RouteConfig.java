package com.example.file.config;

import com.example.file.controller.FileController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration(proxyBeanMethods = false)
public class RouteConfig {

    @Bean
    public RouterFunction<ServerResponse> route(FileController fileController) {
        return RouterFunctions
                .route(
                        GET("/file"), fileController::findAll)
                .andRoute(
                        POST("/file").and(accept(MediaType.APPLICATION_JSON)), fileController::create)
                .andRoute(
                        PUT("/file").and(accept(MediaType.APPLICATION_JSON)), fileController::update)
                .andRoute(
                        DELETE("/file"), fileController::remove);
    }
}
