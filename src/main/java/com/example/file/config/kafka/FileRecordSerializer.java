package com.example.file.config.kafka;

import com.example.common.domain.Record;
import com.example.common.topics.File;
import com.example.service.utils.kafka.JsonValueSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class FileRecordSerializer extends JsonValueSerializer<Record<File>> {
    @Override
    protected Class<Record<File>> getType() {
        return TypeFactory
                .defaultInstance()
                .constructParametricType(Record.class, File.class)
                .getTypeHandler();
    }
}
