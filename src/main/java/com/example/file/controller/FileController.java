package com.example.file.controller;

import com.example.common.topics.File;
import com.example.file.dao.FileDao;
import com.example.file.producer.FileProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

import static com.example.common.domain.Record.CrudType.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class FileController {

    private final FileProducer producer;

    private final FileDao fileDao;

    public Mono<ServerResponse> findAll(ServerRequest request) {
        return Mono
                .from(fileDao
                        .findAll()
                        .collect(Collectors.toList()))
                .flatMap(files ->
                        ServerResponse
                                .status(HttpStatus.OK)
                                .bodyValue(files))
                .onErrorResume(e ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(e.getMessage()));
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        return producer
                .send(CREATE, request
                        .bodyToMono(File.class)
                        .doOnNext(file -> log.info("creating file: " + file)))
                .flatMap(o ->
                        ServerResponse
                                .status(HttpStatus.CREATED)
                                .build())
                .onErrorResume(e ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(e.getMessage()));
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        return producer
                .send(UPDATE, request
                        .bodyToMono(File.class)
                        .doOnNext(file -> log.info("updating file: " + file)))
                .flatMap(unused ->
                        ServerResponse
                                .status(HttpStatus.OK)
                                .build())
                .onErrorResume(error ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(error.getMessage()));
    }

    public Mono<ServerResponse> remove(ServerRequest request) {
        return producer
                .send(DELETE, request
                        .queryParam("id")
                        .map(id -> File.builder().id(id).build())
                        .map(Mono::just)
                        .orElseGet(() -> Mono.error(new IllegalArgumentException("ID is required"))))
                .flatMap(unused ->
                        ServerResponse
                                .status(HttpStatus.ACCEPTED)
                                .build())
                .onErrorResume(error ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(error.getMessage()));
    }
}
